-- 1) Group by attributes (first + last name) and determine the group size.
-- Group size = amount of records for each unique first + last name combination.
CREATE materialized view ncvd_attr_grp_sz AS
SELECT first_name, last_name, COUNT(*) AS group_size FROM ncvd_plausible np 
GROUP BY first_name, last_name
ORDER BY group_size;

-- 2) Count group sizes and determine the total amount of records that every group size contains.
CREATE VIEW ncvd_attr_grp_rec_cnt AS 
SELECT COUNT(*) as group_cnt, group_size, group_size * COUNT(*) AS total_cnt FROM ncvd_attr_grp_sz nagz
GROUP BY group_size;

-- 3) Weigh group sizes.
-- We want (100 + 3*4900) records in total in our result.
-- Since we have five group sizes to consider, we need to divide every total record
-- count for every group by five to get a "per record" weight for a group size.
CREATE VIEW ncvd_attr_grp_wght_5 AS
SELECT nagrc.*, ((100 + 3*4900.0) / total_cnt / 5) as per_record_weight 
FROM ncvd_attr_grp_rec_cnt nagrc
WHERE group_size <= 5;

-- 4) Determine groups to be included in the final selection.
SELECT SETSEED(0.727);

CREATE MATERIALIZED VIEW ncvd_balanced_attr_5 AS
SELECT first_name, last_name, group_size FROM (
	SELECT nags.first_name, nags.last_name, nags.group_size, nagw.per_record_weight, RANDOM() AS rand FROM ncvd_attr_grp_sz nags 
	JOIN ncvd_attr_grp_wght_5 nagw 
	ON nags.group_size = nagw.group_size 
	WHERE nags.group_size <= 5
) tab_wght
WHERE rand < per_record_weight;

-- 5) Validate group sizes (sum of proj_rec_cnt should be close to target record count)
SELECT group_size, COUNT(*) as group_cnt, COUNT(*) * group_size as proj_rec_cnt
FROM ncvd_balanced_attr_5 nbab 
GROUP BY group_size
ORDER BY group_size ASC;
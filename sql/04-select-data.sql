SELECT SETSEED(0.272);

-- 1) Select all records that are listed in the final group table.
CREATE MATERIALIZED VIEW ncvd_rl_in_pht AS
SELECT np.* FROM ncvd_plausible np 
JOIN ncvd_balanced_attr_5 nbab 
ON np.first_name = nbab.first_name AND np.last_name = nbab.last_name
ORDER BY RANDOM();

-- 2) Validate group sizes.
SELECT group_size, COUNT(*) as group_cnt, COUNT(*) * group_size AS rec_cnt FROM (
	SELECT first_name, last_name, COUNT(*) AS group_size FROM ncvd_rl_in_pht 
	GROUP BY first_name, last_name
) grp_tab
GROUP BY group_size
ORDER BY group_size;

-- 3) Export records that are present in all files.
-- Note that this will have to be executed thrice for every NCID prefix.
SELECT '0-' || ncid AS ncid, first_name, last_name, gender_code, birth_year, res_city_desc 
FROM ncvd_rl_in_pht nripb
LIMIT 100;

SELECT '1-' || ncid AS ncid, first_name, last_name, gender_code, birth_year, res_city_desc 
FROM ncvd_rl_in_pht nripb
LIMIT 100;

SELECT '2-' || ncid AS ncid, first_name, last_name, gender_code, birth_year, res_city_desc 
FROM ncvd_rl_in_pht nripb
LIMIT 100;

-- 4) Assign random file number to every record that is not part of all files.
SELECT SETSEED(0.222);

CREATE MATERIALIZED VIEW ncvd_rl_in_pht_file_idx AS
SELECT FLOOR(RANDOM() * 3) as file_idx, tab_skip.* FROM (
	SELECT * FROM ncvd_rl_in_pht nripb 
	OFFSET 100
) tab_skip;

-- 5) Export records for each file.
SELECT file_idx || '-' || ncid AS ncid, first_name, last_name, gender_code, birth_year, res_city_desc
FROM ncvd_rl_in_pht_file_idx
WHERE file_idx = 0;

SELECT file_idx || '-' || ncid AS ncid, first_name, last_name, gender_code, birth_year, res_city_desc
FROM ncvd_rl_in_pht_file_idx
WHERE file_idx = 1;

SELECT file_idx || '-' || ncid AS ncid, first_name, last_name, gender_code, birth_year, res_city_desc
FROM ncvd_rl_in_pht_file_idx
WHERE file_idx = 2;
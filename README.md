# PPRL in PHT NCVR provisioning

This repository contains the SQL code for preparing and selecting data for privacy-preserving record linkage as published in our publications.
All scripts are guaranteed to work with Postgres servers.
The scripts are contained in the [sql directory](./sql/).

[You can obtain a copy of North Carolina Voter Registration Data from the official North Carolina State Board of Elections](https://www.ncsbe.gov/results-data/voter-registration-data).
Extract it to a known location alongside your Postgres installation.
Run the [table creation script](./sql/01-setup-ncvd.sql), then fill it with data from a `psql` shell using the following command.

```
\COPY ncvd FROM '/path/to/ncvd.txt' ENCODING 'LATIN1' CSV DELIMITER E'\t' QUOTE '"' HEADER NULL '';
```

Then continue with the following scripts.
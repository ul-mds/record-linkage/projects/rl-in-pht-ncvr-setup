-- Based on the structure defined at https://s3.amazonaws.com/dl.ncsbe.gov/data/layout_ncvoter.txt.
-- Small alterations done to accommodate real data and simplify queries.
CREATE TABLE ncvd (
    county_id int,
    county_desc varchar(15),
    voter_reg_num char(12),
    ncid char(12),
    last_name varchar(25),
    first_name varchar(20),
    middle_name varchar(20),
    name_suffix_lbl char(4),
    status_cd char(2),
    voter_status_desc varchar(25),
    reason_cd char(2),
    voter_status_reason_desc varchar(60),
    res_street_address varchar(65),
    res_city_desc varchar(60),
    state_cd varchar(2),
    zip_code char(9),
    mail_addr1 varchar(40),
    mail_addr2 varchar(40),
    mail_addr3 varchar(40),
    mail_addr4 varchar(40),
    mail_city varchar(30),
    mail_state varchar(2),
    mail_zipcode char(9),
    full_phone_number varchar(12),
    confidential_ind char(1),
    registr_dt date,
    race_code char(3),
    ethnic_code char(3),
    party_cd char(3),
    gender_code char(1),
    birth_year int,
    age_at_year_end int,
    birth_state varchar(2),
    drivers_lic char(1),
    precinct_abbrv varchar(6),
    precinct_desc varchar(60),
    municipality_abbrv varchar(6),
    municipality_desc varchar(60),
    ward_abbrv varchar(6),
    ward_desc varchar(60),
    cong_dist_abbrv varchar(6),
    super_court_abbrv varchar(6),
    judic_dist_abbrv varchar(6),
    nc_senate_abbrv varchar(6),
    nc_house_abbrv varchar(6),
    county_commiss_abbrv varchar(6),
    county_commiss_desc varchar(60),
    township_abbrv varchar(6),
    township_desc varchar(60),
    school_dist_abbrv varchar(6),
    school_dist_desc varchar(60),
    fire_dist_abbrv varchar(6),
    fire_dist_desc varchar(60),
    water_dist_abbrv varchar(6),
    water_dist_desc varchar(60),
    sewer_dist_abbrv varchar(6),
    sewer_dist_desc varchar(60),
    sanit_dist_abbrv varchar(6),
    sanit_disc_desc varchar(60),
    rescue_dist_abbrv varchar(6),
    rescue_dist_desc varchar(60),
    munic_dist_abbrv varchar(6),
    munic_dist_desc varchar(60),
    dist_1_abbrv varchar(6),
    dist_1_desc varchar(60),
    vtd_abbrv varchar(6),
    vtd_desc varchar(60)
);
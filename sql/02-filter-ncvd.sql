CREATE OR REPLACE MATERIALIZED VIEW ncvd_plausible AS SELECT * FROM ncvd n 
	-- keep records that are active and verified
	WHERE reason_cd = 'AV'
	-- keep records of people that were old enough to register on their registration date
	AND extract(YEAR FROM registr_dt) >= birth_year + 16
	-- keep records of people who are at most 120 years old
	AND age_at_year_end <= 120
	-- keep records that are not confidential
	AND confidential_ind = 'N'
	-- keep records that don't have all zeros as their zip codes
	AND zip_code <> '00000'
	AND mail_zipcode <> '00000'
	-- keep records that have a regular zip or zip+4
	AND LENGTH(zip_code) IN (5, 9)
	AND LENGTH(mail_zipcode) IN (5, 9)
	-- keep records that don't have all zeros as their phone numbers
	AND full_phone_number !~ '^0+$';